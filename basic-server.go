package main

import (
	"io"
	"net/http"
	"os"

	"github.com/gin-gonic/contrib/cors"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
)

func main() {
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	router := gin.Default()

	router.Use(cors.Default())

	router.Use(static.Serve("/", static.LocalFile("./views", true)))

	// Setup route group for the API
	api := router.Group("/api")
	{
		api.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"message": "pong",
			})
		})

		api.GET("/uptime", UptimeHandler)
		api.GET("/starttime", StartTimeHandler)
		api.GET("/ip", ClientIPHandler)
		api.GET("/uniqueip", IPAddressHandler)

	}

	router.Run(":80")
}
